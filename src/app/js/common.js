$(function() {
  $(document).ready(function() {
    $(window).on("load", function() {
      $(".flex-container").masonry({
        itemSelector: ".item",
        columnWidth: 310
      });
    });

    $(".inner-item").magnificPopup({
      type: "image",
      closeBtnInside: false,
      closeOnContentClick: true,
      tLoading: "",

      callbacks: {
        beforeChange: function() {
          this.items[0].src = this.items[0].src + "?=" + Math.random();
        }
      }
    });

    $(".skillbar").skillBars({
      from: 0,
      speed: 4000,
      interval: 100,
      decimals: 0
    });
    var $main = $(".main");
    var $title = $main.find(".title");
    var $underTitle = $main.find(".under-title");

    var title = {
      element: $title,
      newText: "Welcome to Neyron's portfolio."
    };
    var underTitle = {
      element: $underTitle,
      newText: "Modern Frontend developer."
    };

    $(window).on("load", function() {
      bubbleText(title);
      bubbleText(underTitle);
      $(".main")
        .find(".buttons-block")
        .addClass("active");
    });

    var lastAnimation = 0;
    $(".up_arrow").on("click", function(event) {
      var timeNow = Date.now();

      if (timeNow - lastAnimation < 600) {
        return event.preventDefault();
      }
      var target = $($(this).attr("href"));
      event.preventDefault();
      $("html, body").animate(
        {
          scrollTop: target.offset().top
        },
        600
      );
      lastAnimation = timeNow;
    });
  });
});
